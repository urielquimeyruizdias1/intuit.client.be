﻿using Intuit.Client.BE.Models.DTOs;
using Intuit.Client.BE.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace Intuit.Client.BE.Models.Profiles
{
	public class ClienteProfile : Profile
	{
		public ClienteProfile()
		{
			CreateMap<Cliente, ClienteDTO>()
				.ForMember(destino => destino.IdCliente, option => option.MapFrom(origen => origen.IdCliente))
				.ForMember(destino => destino.Nombre, option => option.MapFrom(origen => origen.Nombre))
				.ForMember(destino => destino.Apellido, option => option.MapFrom(origen => origen.Apellido))
				.ForMember(destino => destino.Celular, option => option.MapFrom(origen => origen.Celular))
				.ForMember(destino => destino.Domicilio, option => option.MapFrom(origen => origen.Domicilio))
				.ForMember(destino => destino.Email, option => option.MapFrom(origen => origen.Email))
				.ForMember(destino => destino.CUIT, option => option.MapFrom(origen => origen.CUIT))
				.ForMember(destino => destino.FechaNacimiento, option => option.MapFrom(origen => origen.FechaNacimiento))
				.ReverseMap();
		}
	}
}
