﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intuit.Client.BE.Models.DTOs
{
	public class ClienteDTO
	{
		public int IdCliente { get; set; }
		public string Nombre { get; set; }
		public string Apellido { get; set; }
		public DateTime FechaNacimiento { get; set; }
		public string Domicilio { get; set; }
		public string Celular { get; set; }
		public string CUIT { get; set; }
		public string Email { get; set; }
	}
}
