﻿using AutoMapper;
using Intuit.Client.BE.Models.Profiles;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intuit.Client.BE.Models
{
	public static class DependencyInjection
	{
		public static IServiceCollection AddMaps(this IServiceCollection services)
		{
			var mapperConfig = new MapperConfiguration(m =>
			{
				m.AddProfile(new ClienteProfile());
			});
			IMapper mapper = mapperConfig.CreateMapper();
			services.AddSingleton(mapper);

			return services;
		}
	}
}
