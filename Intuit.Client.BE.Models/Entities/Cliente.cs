﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Intuit.Client.BE.Models.Entities
{
	[Table(name: "TBL_Cliente")]

	public class Cliente
	{
		[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Required(ErrorMessage = "El campo {0} es requerido")]
		public int IdCliente { get; set; }
		[Column("Nombre")]
		[Required(ErrorMessage = "El campo {0} es requerido")]
		public string Nombre { get; set; }
		[Column("Apellido")]
		[Required(ErrorMessage = "El campo {0} es requerido")]
		public string Apellido { get; set; }
		[Column("FechaNacimiento")]
		[Required(ErrorMessage = "El campo {0} es requerido")]
		public DateTime FechaNacimiento { get; set; }
		[Column("Domicilio")]
		public string Domicilio { get; set; }
		[Column("Celular")]
		public string Celular { get; set; }
		[Column("CUIT")]
		public string CUIT { get; set; }
		[Column("Email")]
		public string Email { get; set; }
	}
}