﻿using Intuit.Client.BE.DataAccess;
using Intuit.Client.BE.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intuit.Client.BE.Repositories.Implements
{
	public class BaseRepository<TEntity> : IRepository<TEntity>
	   where TEntity : class
	{
		#region Dependencias
		protected readonly ClientDBContext _dbContext;
		protected readonly DbSet<TEntity> _dbSet;
		#endregion

		#region Constructor
		public BaseRepository(
			ClientDBContext context
			)
		{
			_dbContext = context;
			_dbSet = _dbContext.Set<TEntity>();
		}
		#endregion

		#region Metodos
		public IEnumerable<TEntity> GetAll()
		{
			return _dbSet.AsEnumerable();
		}
		public IQueryable<TEntity> GetQueryable()
		{
			return _dbSet.AsNoTracking();
		}

		public TEntity Insert(TEntity entity)
		{
			_dbContext.Add(entity);
			return entity;
		}

		public TEntity Update(TEntity entity)
		{
			_dbContext.Update(entity);
			return entity;
		}
		#endregion
	}
}

