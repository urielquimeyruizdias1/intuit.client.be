﻿using Intuit.Client.BE.DataAccess;
using Intuit.Client.BE.Models.Entities;
using Intuit.Client.BE.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Intuit.Client.BE.Repositories.Implements
{
    public class ClienteRepository : BaseRepository<Cliente>, IClienteRepository, IRepository<Cliente>
	{
		public ClienteRepository(ClientDBContext context) : base(context)
		{
		}

		

		public Cliente GetBy(int idCliente)
		{
			return _dbContext.Cliente.FirstOrDefault(c => c.IdCliente == idCliente);
		}


		public Cliente SearchBy(string nombre)
		{
			return _dbContext.Cliente.FirstOrDefault(c => c.Nombre.ToLower() == nombre.ToLower());
		}



	}
}

