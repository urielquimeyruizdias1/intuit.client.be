﻿using Intuit.Client.BE.DataAccess;
using Intuit.Client.BE.Repositories.Implements;
using Intuit.Client.BE.Repositories.Interfaces;
using Intuit.Client.BE.Repositories.UoW;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intuit.Client.BE.Repositories.Support
{
	public static class DatabaseExtension
	{
		/// <summary>
		/// Metodo extensivo para la configuracion de la base de datos
		/// </summary>
		/// <param name="services"></param>
		/// <param name="Configuration"></param>
		/// <returns></returns>
		public static IServiceCollection AddRepositoryClient(this IServiceCollection services, IConfiguration Configuration)
		{
			#region Unit Of Work
			services.AddTransient<IUnitOfWork, UnitOfWork>();
			#endregion

			#region DbContext
			services.AddDbContext<ClientDBContext>(options =>
			options.UseSqlServer(Configuration.GetConnectionString("CLIENT_DB")));
			#endregion


			#region Repositories
			services.AddTransient<IClienteRepository, ClienteRepository>();

			#endregion
			return services;
		}
	}
}

