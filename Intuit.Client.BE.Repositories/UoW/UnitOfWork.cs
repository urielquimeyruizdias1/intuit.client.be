﻿using Intuit.Client.BE.DataAccess;
using Intuit.Client.BE.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intuit.Client.BE.Repositories.UoW
{
	public class UnitOfWork : IUnitOfWork
	{
		#region Dependencias
		private readonly ClientDBContext _context;
		public IClienteRepository Cliente { get; }

		#endregion

        public UnitOfWork(ClientDBContext context,IClienteRepository clienteRepository)
        {
			this._context = context;
			this.Cliente = clienteRepository;
            
        }
		#region Metodos
		public async Task SaveChanges()
		{
			await _context.SaveChangesAsync();
		}
		public void ChangeTrackerClear()
		{
			_context.ChangeTracker.Clear();
		}
		#endregion
	}
}
