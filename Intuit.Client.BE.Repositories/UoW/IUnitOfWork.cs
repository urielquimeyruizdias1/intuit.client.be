﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Intuit.Client.BE.Repositories.Interfaces;

namespace Intuit.Client.BE.Repositories.UoW
{
    public interface IUnitOfWork
	{
		IClienteRepository Cliente { get; }
		Task SaveChanges();
		void ChangeTrackerClear();
	}
}
