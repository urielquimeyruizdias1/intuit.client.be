﻿using Intuit.Client.BE.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intuit.Client.BE.Repositories.Interfaces
{
	public interface IRepository<TEntity> where TEntity : class
	{
		IEnumerable<TEntity> GetAll();
		IQueryable<TEntity> GetQueryable();
		TEntity Insert(TEntity entity);
		TEntity Update(TEntity idEntity);
	}
}
