﻿using Intuit.Client.BE.Models.Entities;

namespace Intuit.Client.BE.Repositories.Interfaces
{
    public interface IClienteRepository : IRepository<Cliente>
    {
		Cliente GetBy(int idCliente);
		Cliente SearchBy(string nombreCliente);
		
    }
}
