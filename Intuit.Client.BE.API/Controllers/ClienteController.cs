using Intuit.Client.BE.Models.DTOs;
using Intuit.Client.BE.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Intuit.Client.BE.API.Controllers
{
	[ApiController]

	[Route("api/[controller]")]
	public class ClienteController : ControllerBase
	{
		private readonly ILogger<ClienteController> _logger;

		//public ClienteController(ILogger<ClienteController> logger)
		//{
		//	_logger = logger;
		//}

		#region Dependencias
		private readonly IClienteService _clienteService;
		#endregion

		#region Constructor
		public ClienteController(
			IClienteService clienteService
			)
		{
			_clienteService = clienteService;
		}
		#endregion

		#region Endpoints
		/// <summary>
		/// Cliente
		/// </summary>
		/// <remarks>
		/// Inserta un cliente 
		/// </remarks>
		/// <returns></returns>
		/// <response code="200">Devuelve la entidad</response>
		/// <response code="204">Vacio</response>
		/// <response code="500">En caso de alguno error interno</response>
		[SwaggerOperation(Summary = "Inserta un cliente")]
		[HttpPost("Insert")]
		[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ClienteDTO))]
		[ProducesResponseType(StatusCodes.Status204NoContent)]
		[ProducesResponseType(StatusCodes.Status500InternalServerError)]
		public async Task<IActionResult> Insert(ClienteDTO clienteDTO)
		{
			var list = await _clienteService.Insert(clienteDTO);
			return Ok(list);
		}
		/// <summary>
		/// Cliente
		/// </summary>
		/// <remarks>
		/// Obtiene un cliente por su nombre
		/// </remarks>
		/// <returns></returns>
		/// <response code="200">Devuelve la entidad</response>
		/// <response code="204">Vacio</response>
		/// <response code="500">En caso de alguno error interno</response>
		[SwaggerOperation(Summary = "Actualiza un cliente")]
		[HttpPut("Update")]
		[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ClienteDTO))]
		[ProducesResponseType(StatusCodes.Status204NoContent)]
		[ProducesResponseType(StatusCodes.Status500InternalServerError)]
		public async Task<IActionResult> Update(ClienteDTO cliente)
		{
			var list = await _clienteService.Update(cliente);
			return Ok(list);
		}
		/// <summary>
		/// Cliente
		/// </summary>
		/// <remarks>
		/// Obtiene un cliente por su nombre
		/// </remarks>
		/// <returns></returns>
		/// <response code="200">Devuelve la entidad</response>
		/// <response code="204">Vacio</response>
		/// <response code="500">En caso de alguno error interno</response>
		[SwaggerOperation(Summary = "Obtiene un cliente por su nombre")]
		[HttpGet("SearchBy")]
		[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ClienteDTO))]
		[ProducesResponseType(StatusCodes.Status204NoContent)]
		[ProducesResponseType(StatusCodes.Status500InternalServerError)]
		public async Task<IActionResult> SearchBy(string nombre)
		{
			var list = await _clienteService.SearchBy(nombre);
			return Ok(list);
		}
		/// <summary>
		/// Cliente
		/// </summary>
		/// <remarks>
		/// Obtiene los Clientes 
		/// </remarks>
		/// <returns></returns>
		/// <response code="200">Devuelve la entidad</response>
		/// <response code="204">Vacio</response>
		/// <response code="500">En caso de alguno error interno</response>
		[SwaggerOperation(Summary = "Obtiene un cliente por IdCliente")]
		[HttpGet("GetBy")]
		[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ClienteDTO))]
		[ProducesResponseType(StatusCodes.Status204NoContent)]
		[ProducesResponseType(StatusCodes.Status500InternalServerError)]
		public async Task<IActionResult> GetBy(int idCliente)
		{
			var list = await _clienteService.GetBy(idCliente);
			return Ok(list);
		}
		/// <summary>
		/// Cliente
		/// </summary>
		/// <remarks>
		/// Obtiene los Clientes 
		/// </remarks>
		/// <returns></returns>
		/// <response code="200">Devuelve la entidad</response>
		/// <response code="204">Vacio</response>
		/// <response code="500">En caso de alguno error interno</response>
		[SwaggerOperation(Summary = "Obtiene la lista de Clientes")]
		[HttpGet("GetAll")]
		[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ClienteDTO))]
		[ProducesResponseType(StatusCodes.Status204NoContent)]
		[ProducesResponseType(StatusCodes.Status500InternalServerError)]
		public async Task<IActionResult> GetAll()
		{
			var list = await _clienteService.GetAll();
			return Ok(list);
		}
		#endregion


	}
}