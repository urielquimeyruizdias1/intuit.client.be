using Intuit.Client.BE.API.Support;
using Intuit.Client.BE.Models;
using Intuit.Client.BE.Repositories.Support;
using Intuit.Client.BE.Services.Support;
namespace Intuit.Client.BE.API
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{

			services.AddCors(options => options.AddPolicy("AllowAccess_To_API",
				policy => policy
				.AllowAnyOrigin()
				.AllowAnyMethod()
				.AllowAnyHeader()
				));

			services.AddSwaggerGen();
			services.AddRepositoryClient(Configuration);
			services.AddInternalServices(Configuration);
			services.AddMaps();
			services.AddControllers();
		}
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			app.UseCors("AllowAccess_To_API");
			app.UseDeveloperExceptionPage();
			app.UseCustomizedSwagger();

			//app.UseHsts();
			//app.UseCustomizedSwagger();

			//app.UseCustomizedExceptionMiddleware();
			//app.UseExceptionHandler(appError =>
			//	appError.UseCustomizedExceptionHandler()
			//	);

			app.UseHttpsRedirection();

			app.UseRouting();

			// siempre primero Authentication
			// ... y ambos siempre deben estar luego de UseRouting
			app.UseAuthentication();
			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}

	}
}