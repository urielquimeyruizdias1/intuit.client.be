﻿namespace Intuit.Client.BE.API.Support
{
	public static class SwaggerExtension
	{
		/// <summary>
		/// Metodo que extiende para la utilizacion del app
		/// </summary>
		/// <param name="app"></param>
		/// <returns></returns>
		public static IApplicationBuilder UseCustomizedSwagger(
			this IApplicationBuilder app)
		{
			app.UseSwagger();
			app.UseSwaggerUI(options =>
			{
				options.SwaggerEndpoint("/swagger/v1/swagger.json", "INDEC.ENGE.API v1");
			});

			return app;
		}
	}
}
