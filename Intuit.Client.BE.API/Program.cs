using Intuit.Client.BE.API;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

CreateHostBuilder(args).Build().Run();
static IHostBuilder CreateHostBuilder(string[] args) =>
	Host.CreateDefaultBuilder(args)
		.ConfigureWebHostDefaults(webBuilder =>
		{
			webBuilder.UseStartup<Startup>();
		});
//var builder = WebApplication.CreateBuilder(args);

//// Add services to the container.

//builder.Services.AddControllers();
//// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
//builder.Services.AddEndpointsApiExplorer();
//builder.Services.AddSwaggerGen();
//void ConfigureServices(IServiceCollection services)
//{

//	services.AddCors(options => options.AddPolicy("AllowAccess_To_API",
//		policy => policy
//		.AllowAnyOrigin()
//		.AllowAnyMethod()
//		.AllowAnyHeader()
//		));

//	services.AddRepositoryClient(Configuration);
//	// Se mapea de json a SnapShotSettings

//	services.AddControllers();
//}


//var app = builder.Build();

//// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
//	app.UseSwagger();
//	app.UseSwaggerUI();
//}

//app.UseHttpsRedirection();

//app.UseAuthorization();

//app.MapControllers();

//app.Run();
