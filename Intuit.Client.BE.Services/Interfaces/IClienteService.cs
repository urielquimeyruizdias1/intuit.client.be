﻿using Intuit.Client.BE.Models.DTOs;
using Intuit.Client.BE.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intuit.Client.BE.Services.Interfaces
{
	public interface IClienteService
	{
		public Task<ClienteDTO> SearchBy(string nombre);
		public Task<ClienteDTO> GetBy(int idCliente);
		public Task<ClienteDTO> Insert(ClienteDTO cliente);
		public Task<ClienteDTO> Update(ClienteDTO cliente);
		public Task<IEnumerable<ClienteDTO>> GetAll();
	}
}
