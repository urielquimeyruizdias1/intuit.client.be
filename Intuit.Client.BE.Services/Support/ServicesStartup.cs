﻿using Intuit.Client.BE.Services.Implements;
using Intuit.Client.BE.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intuit.Client.BE.Services.Support
{
	public static class ServicesStartup
	{
		/// <summary>
		/// Metodo extensivo para la configuracion de los services
		/// </summary>
		/// <param name="services"></param>
		/// <param name="Configuration"></param>
		/// <returns></returns>
		public static IServiceCollection AddInternalServices(this IServiceCollection services, IConfiguration Configuration)
		{
			services.AddTransient<IClienteService, ClienteService>();

			return services;
		}
	}
}
