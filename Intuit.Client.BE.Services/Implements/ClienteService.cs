﻿using AutoMapper;
using Intuit.Client.BE.Models.DTOs;
using Intuit.Client.BE.Models.Entities;
using Intuit.Client.BE.Repositories.UoW;
using Intuit.Client.BE.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Intuit.Client.BE.Services.Implements
{
	public class ClienteService : IClienteService
	{
		private readonly IMapper _mapper;
		private readonly IUnitOfWork _unitOfWork;
		public ClienteService(IUnitOfWork unitOfWork, IMapper mapper)
		{
			_unitOfWork = unitOfWork;
			_mapper = mapper;
		}
		public async Task<IEnumerable<ClienteDTO>> GetAll()
		{
			var list = _unitOfWork.Cliente.GetAll();
			var listDTO = _mapper.Map<IEnumerable<ClienteDTO>>(list);
			return listDTO;
		}

		public async Task<ClienteDTO> GetBy(int idCliente)
		{
			var cliente = _unitOfWork.Cliente.GetBy(idCliente);
			return _mapper.Map<ClienteDTO>(cliente);
		}

		public async Task<ClienteDTO> Insert(ClienteDTO cliente)
		{
			if (ValidateCuit(cliente.CUIT))
			{
				var clienteEnt = _mapper.Map<Cliente>(cliente);
				var clienteInsert = _unitOfWork.Cliente.Insert(clienteEnt);
				await _unitOfWork.SaveChanges();
				return _mapper.Map<Cliente, ClienteDTO>(clienteInsert);
			}
			return null;
		}

		public bool ValidateCuit(string cuit)
		{
			if (string.IsNullOrEmpty(cuit)) throw new ArgumentNullException(nameof(cuit));
			if (cuit.Length != 11) throw new ArgumentException(nameof(cuit));
			bool rv = false;
			int verificador;
			int resultado = 0;
			string codes = "6789456789";
			long cuit_long = 0;
			if (long.TryParse(cuit, out cuit_long))
			{
				verificador = int.Parse(cuit[cuit.Length - 1].ToString());
				int x = 0;
				while (x < 10)
				{
					int digitoValidador = int.Parse(codes.Substring((x), 1));
					int digito = int.Parse(cuit.Substring((x), 1));
					int digitoValidacion = digitoValidador * digito;
					resultado += digitoValidacion;
					x++;
				}
				resultado = resultado % 11;
				rv = (resultado == verificador);
			}
			return rv;
		}
		public async Task<ClienteDTO> SearchBy(string nombre)
		{
			var cliente = _unitOfWork.Cliente.SearchBy(nombre);
			return _mapper.Map<ClienteDTO>(cliente);
		}

		public async Task<ClienteDTO> Update(ClienteDTO cliente)
		{
			var clienteEnt = _mapper.Map<Cliente>(cliente);
			var clienteUpdate = _unitOfWork.Cliente.Update(clienteEnt);
			await _unitOfWork.SaveChanges();
			return _mapper.Map<Cliente, ClienteDTO>(clienteUpdate);


		}

	}
}
