﻿using Intuit.Client.BE.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace Intuit.Client.BE.DataAccess
{
    public class ClientDBContext : DbContext
	{
		public ClientDBContext(DbContextOptions<ClientDBContext> options) : base(options) { }
		public DbSet<Cliente> Cliente { get; set; }

	}
}

